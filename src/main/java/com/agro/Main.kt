package com.agro

/*
В зависимости от деталей ТЗ и контекста развития программы,
имеет смысл немного изменить (усложнить) архитектуру (подробнее расскажу на собеседовании).
 Но так как задание тестовое  и довольно небольшое особо сильно заморачиватся с этим не стал.
 */

fun main()
{
    val userCompressor = UserCompressor()
    println("Enter users in format 'userName -> xyz@pisem.net, vasya@pupkin.com'.\n" +
            "Enter empty line to begin compression:")
    val users = ConsoleDataManager.getInDataFromConsole()
    val compressed = userCompressor.compressUsers(users)
    ConsoleDataManager.putUsersToConsole(compressed)
}
