package com.agro

/**
 * Содержит инструменты для того чтобы брать данные из консоли и
 * преобразует их  в требуемый фомат и наоборот..
 */
object ConsoleDataManager {

    private data class User(val name: String, val emalilSet: MutableSet<String>)

    /**
     * Распечатывает пользователей в консоль
     * @param users HashMap<имя пользователя,лист адресов>
     */
    fun putUsersToConsole(users: HashMap<String, MutableSet<String>>)
    {
        users.forEach{
            println(userToLine(it.key,it.value))
        }
    }

    /**
     * Метод принимающий входные данные из консоли.
     *
     * @return HashMap<имя пользователя,лист адресов> введенных пользователей
     */
    fun getInDataFromConsole(): HashMap<String, MutableSet<String>>
    {
        val userMap = HashMap<String, MutableSet<String>>()
        var line = ""
        while (true)
        {
            try {
                line = readLine()!!
                if (line.isEmpty())
                    break

                val user = getUserFromLine(line)
                if(!userMap.containsKey(user.name))
                    userMap.put(user.name, user.emalilSet)
                else
                    println("User with this name exists.")
            }
            catch (ex: Exception)
            {
                println("Error print data in format 'userName -> xyz@pisem.net, vasya@pupkin.com'")
            }
        }
        return userMap
    }

    private fun userToLine(name: String, emailSet: MutableSet<String>) : String
    {
        var line = "$name -> "
        emailSet.forEach { line += "$it, " }
        line = line.removeRange(line.length - 2, line.length-1)
        return line
    }






    private fun getUserFromLine(line: String):User
    {
        fun getNameFromLine(line: String) : String
        {
            return  line!!.split("->")[0].replace(" ", "")
        }

        fun getEmailSetFromLine(line: String) : MutableSet<String>
        {
            val emailList = line!!.split("->")[1].split(",").toMutableSet()
            emailList.forEach { it.replace(" ", "") }
            return emailList
        }

        val userName = getNameFromLine(line!!)
        val emailSet =getEmailSetFromLine(line)
        return User(userName, emailSet)
    }
}