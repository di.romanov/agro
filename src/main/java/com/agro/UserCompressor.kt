package com.agro


class UserCompressor {


    /**
     * Сжать список пользователей.
     * При нахождении одинаковых адресов у разных пользователей объединяет полььзователей под
     * именем одного из них. Список почт также объединяется.
     */
    fun compressUsers(users : HashMap<String, MutableSet<String>>): HashMap<String, MutableSet<String>>
    {
        val compressed = HashMap<String, MutableSet<String>>()

        users.forEach { (name, mailSet) ->
            val haveUser = compressed.any{it.value.any {mail-> mailSet.contains(mail) }}
            if(haveUser)
            {
                compressed.filter{
                    it.value.any {mail-> mailSet.contains(mail) }
                }.forEach { (n, ms) -> ms.addAll(mailSet) }
            }
            else
            {
                compressed.put(name, mailSet)
            }
        }

        return compressed

    }


}