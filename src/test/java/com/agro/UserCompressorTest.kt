package com.agro

import junit.framework.TestCase

class UserCompressorTest() : TestCase() {

    fun testSmoke()
    {
        val users = HashMap<String, MutableSet<String>>()
        users.put("user1", mutableSetOf("xxx@ya.ru", "foo@gmail.com", "lol@mail.ru"))
        users.put("user2", mutableSetOf("foo@gmail.com", "ups@pisem.net"))
        users.put("user3", mutableSetOf("xyz@pisem.net", "vasya@pupkin.com"))
        users.put("user4", mutableSetOf("ups@pisem.net", "aaa@bbb.ru"))
        users.put("user5", mutableSetOf("xyz@pisem.net"))

        val userCompressor = UserCompressor()
        val  compressedUsers = userCompressor.compressUsers(users)
        assert(compressedUsers.size==2)
    }
}